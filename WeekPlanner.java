import java.util.Scanner;

public class WeekPlanner {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "gym; read a book";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "grocery shopping; clean the house";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "attend a webinar; work on project";
        schedule[5][0] = "Friday";
        schedule[5][1] = "movie night; dinner with friends";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "hiking; family time";

        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Please, input the day of the week: ");
            String input = sc.nextLine().trim().toLowerCase();

            if (input.equals("exit")) {
                break;
            }

            String[] words = input.split("\\s+");
            if (words.length == 2 && (words[0].equals("change") || words[0].equals("reschedule"))) {
                boolean validDay = false;
                for (int i = 0; i < schedule.length; i++) {
                    if (words[1].equals(schedule[i][0].toLowerCase())) {
                        System.out.println("Enter new tasks for " + schedule[i][0] + ":");
                        String updatedTasks = sc.nextLine().trim();
                        schedule[i][1] = updatedTasks;
                        System.out.println("Tasks updated for " + schedule[i][0]);
                        validDay = true;
                        break;
                    }
                }
                if (!validDay) {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
                continue;
            }

            boolean validDay = false;
            for (int i = 0; i < schedule.length; i++) {
                if (schedule[i][0].toLowerCase().equals(input)) {
                    System.out.println("Your tasks for " + schedule[i][0] + ": " + schedule[i][1]);
                    validDay = true;
                    break;
                }
            }

            if (!validDay) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }

        sc.close();
    }
}
